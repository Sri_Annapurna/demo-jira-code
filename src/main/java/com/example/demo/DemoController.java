package com.example.demo;


import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.atlassian.connect.spring.AtlassianHostUser;

@Controller
public class DemoController {

	@RequestMapping("/welcome")
    public String main(Model model,@AuthenticationPrincipal AtlassianHostUser hostUser) {
        model.addAttribute("name", "Trishna");
        return "welcome"; 
    }
}
